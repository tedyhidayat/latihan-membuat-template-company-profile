$(function(){

   	// Scroll to top button appear
	$(document).ready(function() {
		$(window).scroll(function(){
			if ($(window).scrollTop() > 300) {
				$('#btn-scrolltotop').fadeIn();
			} else {
				$('#btn-scrolltotop').fadeOut();
			}
		});
	});

	// Smooth scrolling using jQuery easing
	$(document).on('click', '#btn-scrolltotop', function(e) {
	  var $anchor = $(this);
	  $('html, body').stop().animate({scrollTop: 0}, 1000, 'easeInOutExpo');
	  e.preventDefault();
	});

	// end scroll to top

	// owl-carousel
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		animateOut: 'rollOut',
    	animateIn: 'flipInX',
    	smartSpeed:400,
    	stagePadding:0,
	    loop:true,
	    margin:30,
	    dots:true,
	    autoplay:true,
	    autoplayTimeout:1800,
	    autoplayHoverPause:true,
	    responsiveClass:true,
	        responsive:{
	            0:{
	                items:1,
	                nav:true,
	                stagePadding:0
	            },
	            600:{
	                items:2,
	                nav:false,
	                stagePadding:40
	            },
	            1000:{
	                items:3,
	                nav:false,
	                dots:true
	            }
	        }
	});
	// end owl-carousel

});
